//
//  BaseTableViewCell.swift
//  FilterSearch
//
//  Created by Mac User on 10/1/19.
//

import UIKit

class BaseTableViewCell<M>: UITableViewCell {
    
//    class var name: String {
//        fatalError("Subclasses need to implement the `name` variable.")
//    }
//    
//    class var identifier: String {
//        fatalError("Subclasses need to implement the `identifier` variable.")
//    }
//    
    open func setData(poModel: M) {
        
    }
}
