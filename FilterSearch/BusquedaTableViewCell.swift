//
//  BusquedaTableViewCell.swift
//  FilterSearch
//
//  Created by Mac User on 9/30/19.
//

import UIKit

struct Team {
    let name: String
}

class BusquedaTableViewCell: BaseTableViewCell<Team> {
    @IBOutlet weak var lblTexto: UILabel!
    var tbView = UITableView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setData(poModel: Team) {
        lblTexto.text = poModel.name
    }
    
    func searchMessage(searchText: String? = ""){
        if searchText != "" && searchText != nil{
            
            let str = searchText!.folding(options: .diacriticInsensitive, locale: NSLocale.current)
            let str2 = lblTexto.text!.folding(options: .diacriticInsensitive, locale: NSLocale.current)
            let re = str.replacingOccurrences(of: "[^A-Za-z0-9]", with: " ", options: .regularExpression).lowercased()
            let re2 = str2.replacingOccurrences(of: "[^A-Za-z0-9]", with: " ", options: .regularExpression).lowercased()
            
            var array2 = [String]()
            var char2: String = ""
            var recorre = 0
            
            var array3 = [String]()
            var char3: String = ""
            var recorre2 = 0
            
            
            for c in re {
                recorre += 1
                if c.isLetter || c.isNumber {
                    char2.append(contentsOf: "\(c)")
                    if recorre == re.count {
                        if re.last != " "{
                            array2.append(char2)
                        }
                    }
                }else {
                    if char2 != ""{
                        array2.append(char2)
                    }
                    array2.append("")
                    char2 = ""
                }
            }
            
            for c in re2 {
                recorre2 += 1
                if c.isLetter || c.isNumber {
                    char3.append(contentsOf: "\(c)")
                    if recorre2 == re2.count {
                        if re2.last != " "{
                            array3.append(char3)
                            //recorre = 0
                        }
                    }
                }else {
                    if char3 != ""{
                        array3.append(char3)
                    }
                    array3.append("")
                    char3 = ""
                }
            }
            
            if !searchText!.isEmpty{
                var myMutableString = NSMutableAttributedString()
                myMutableString = NSMutableAttributedString(string: self.lblTexto.text!)
                
                
                var pos:[Int] = []
                var acumulado = 0
                for k in 0..<array3.count {
                    if (k == 0) {
                        pos.append(k)
                    } else {
                        if array3[k-1] == ""{
                            acumulado += 1
                        }else {
                            acumulado += (array3[k-1].count)
                        }
                        pos.append(acumulado)
                    }
                }
                
                //funcion para pintar el label
                for i in 0..<array3.count {
                    for j in 0..<array2.count {
                        let indx = array3[i].indexDistance(of: array2[j])
                        if indx == 0 {
                            myMutableString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.orange, range: NSRange(location: pos[i],length:array2[j].count))
                            self.lblTexto.attributedText = myMutableString
                        }else {
                            if array2[j] != ""{
                                myMutableString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.clear, range: NSRange(location: pos[i],length:array3[j].count))
                                self.lblTexto.attributedText = myMutableString
                            }
                        }
                    }
                }
            }else {
            }
        }else {
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: self.lblTexto.text!)
            myMutableString.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.clear, range: NSRange(location:0,length: self.lblTexto.text!.count))
            self.lblTexto.attributedText = myMutableString
        }
    }
    
}
