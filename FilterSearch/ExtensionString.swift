//
//  ExtensionString.swift
//  FilterSearch
//
//  Created by Mac User on 10/1/19.
//

import Foundation

extension StringProtocol {
    func indexDistance(of string: Self) -> Int? {
        guard let index = range(of: string)?.lowerBound else { return nil }
        return distance(from: startIndex, to: index)
    }
}
