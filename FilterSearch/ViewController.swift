//
//  ViewController.swift
//  FilterSearch
//
//  Created by Mac User on 9/20/19.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var sb: UISearchBar!
    @IBOutlet weak var tbSearching: UITableView!
    fileprivate var teams: [Team] = []
    var labelTxt = UILabel()
    
    var gsSearchBar = ""{
        didSet{
            self.tbSearching.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teams = [Team(name: "Barca"), Team(name: "Madrid"), Team(name: "Deportivo municipal"), Team(name: "ETC...")]
        tbSearching.register(UINib(nibName: "BusquedaTableViewCell", bundle: nil), forCellReuseIdentifier: "BusquedaTableViewCell")
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        gsSearchBar = searchText
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbSearching.dequeueReusableCell(withIdentifier: "BusquedaTableViewCell") as! BusquedaTableViewCell
        cell.setData(poModel: teams[indexPath.row])
        cell.searchMessage(searchText: gsSearchBar)
        return cell
    }
}
